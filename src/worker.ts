import {ProxyManager} from "./proxy";
import {Tester} from "./tester";
import {config} from "./config";
import {logger} from "./utils";

export async function worker() {
  logger.important(`Worker ${process.pid} is starting...`);
  const proxyManager = await new ProxyManager().init();
  const tester = await new Tester(proxyManager, config.targets).init();
  await tester.run();
  process.exit(1);
}

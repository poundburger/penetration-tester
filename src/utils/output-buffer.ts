import fs from "fs";
import {config} from "../config";

export type IOutputBufferData = string[];

export class OutputBuffer {
  private buffer: IOutputBufferData = [];
  private marks: { [k: string]: number } = {};
  private flushInterval: any;

  constructor(public logFilePath: string) {
  }

  static anyValueToString(data: any): string {
    if (typeof data === "object") {
      try {
        return JSON.stringify(data);
      } catch (err) {
        if (typeof data.length !== "undefined") {
          return `[array, ${data.length}]`;
        }
        return `[object, ${Object.keys(data)}]`;
      }
    }
    if (data.toString) {
      return data.toString();
    }
    return '[could not parse data]';
  }

  write(...args: any[]) {
    this.buffer.push(args.map(OutputBuffer.anyValueToString).join(' ') + '\n');

    if (this.flushInterval) {
      clearTimeout(this.flushInterval);
    }
    this.flushInterval = setTimeout(() => {
      this.flushInterval = null;
      this.flush();
    }, config.misc.outputBufferFlushTimerMs);

    return this;
  }

  async flush() {
    const writeStream = fs.createWriteStream(this.logFilePath, { flags: 'w'});
    this.buffer.forEach(chunk => writeStream.write(chunk));
    return this;
  }

  mark(key = 'def') {
    this.marks[key] = this.buffer.length;
    return this;
  }

  removeMark(key = 'def') {
    delete this.marks[key];
    return this;
  }

  clean(key = 'def') {
    const pos = this.marks[key];
    if (typeof pos !== "undefined") {
      this.buffer.splice(this.marks[key]);
      this.removeAllMarksAfterPos(pos);
    }
    return this;
  }

  private removeAllMarksAfterPos(l: number) {
    Object.keys(this.marks)
        .filter(k => this.marks.k >= l)
        .forEach(k => delete this.marks[k]);
    return this;
  }
}

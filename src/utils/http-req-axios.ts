import axios, {AxiosInstance, AxiosRequestConfig, AxiosResponse} from "axios";
import {config} from "../config";
import {promiseAnyWithValue} from "./promise-any-with-value";

export interface IHttpReqAxiosOptions {
  client: AxiosInstance,
  retries: number,
  concurrentRetries: boolean,
  requestOptions?: AxiosRequestConfig
}

const defaultOptions: IHttpReqAxiosOptions = {
  client: axios,
  retries: 3,
  concurrentRetries: true,
  requestOptions: {
    timeout: config.misc.axiosTimeoutMs
  }
}

export async function httpReqAxios(url: string, optionsInput: Partial<IHttpReqAxiosOptions> = {}): Promise<AxiosResponse | null> {
  const options: IHttpReqAxiosOptions = {...defaultOptions, ...optionsInput};
  const requestOptions = {...defaultOptions.requestOptions, ...options.requestOptions};

  if (options.concurrentRetries) {
    return await promiseAnyWithValue<AxiosResponse>(new Array(options.retries).fill(null).map(async () => {
      try {
        return await options.client.get(url, requestOptions);
      } catch (err) {
        return err.response;
      }
    }), v => !!axiosRespSuccess(v)) ?? null;
  }

  let resp: AxiosResponse;
  for (let i = 0; i < options.retries; i++) {
    try {
      resp = await options.client.get(url, requestOptions);
      if (axiosRespSuccess(resp)) {
        return resp;
      }
    } catch (err) {
    }
  }
  return resp ?? null;
}

export function axiosRespSuccess(resp: AxiosResponse) {
  return resp && (resp.status >= 200) && (resp.status < 300) && resp;
}
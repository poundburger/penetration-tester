import {config} from "../config";

export function formatDate(date: Date) {
  return ` ${date.toLocaleTimeString(config.misc.timeFormatLocale)}`;
}

const timeFormatRules: [number, string][] = [
  [1, 'ms'],
  [1000, 'sec'],
  [60 * 1000, 'min'],
  [60 * 60 * 1000, 'h'],
  [24 * 60 * 60 * 1000, 'day(s)'],
];

export function formatTimeDiff(timeMs: number) {
  let timeFormatIndex = 2;

  for (; (timeFormatIndex < timeFormatRules.length) && (timeMs >= timeFormatRules[timeFormatIndex][0]);
         timeFormatIndex++) {
  }
  timeFormatIndex--;

  const rule1 = timeFormatRules[timeFormatIndex];
  const rule2 = timeFormatRules[timeFormatIndex - 1];
  const v1 = Math.floor(timeMs / rule1[0]);
  const v2 = Math.floor((timeMs - v1 * rule1[0]) / rule2[0]);
  return `${v1}${rule1[1]} ${v2}${rule2[1]}`;
}
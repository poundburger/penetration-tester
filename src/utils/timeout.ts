export async function timeout(timeMs: number = 1): Promise<void> {
  return new Promise(resolve => setTimeout(() => resolve(), timeMs));
}

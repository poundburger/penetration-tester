export async function promiseAnyWithValue<T>(
    promises: (Promise<any>)[],
    testFn: ((T) => boolean) | ((T) => Promise<boolean>) = v => v,
    rejectOnFail: boolean = false
): Promise<T | null> {
  return new Promise(async (resolve, reject) => {
    await Promise.all(promises.map(async promise => {
      const result = await promise;

      if (await testFn(result)) {
        resolve(result);
        return result;
      }
      return;
    }));

    if ( rejectOnFail ) {
      reject(null);
    } else {
      resolve(null);
    }
  });
}

import {promiseAnyWithValue} from "./promise-any-with-value";

test('promiseAnyWithValue', async () => {
  expect(await promiseAnyWithValue([
      new Promise(r => { setTimeout(() => r(false), 10); }),
      new Promise(r => { setTimeout(() => r(0), 15); }),
      new Promise(r => { setTimeout(() => r(42), 20); }),
      new Promise(r => { setTimeout(() => r(12), 25); }),
  ])).toEqual(42);
  const startedMs = Date.now();
  await promiseAnyWithValue([
    new Promise(r => { setTimeout(() => r(false), 100); }),
    new Promise(r => { setTimeout(() => r(true), 5); }),
  ]);
  expect(Date.now() - startedMs).toBeLessThan(50);
});
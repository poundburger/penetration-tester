import {config} from "../config";
import {formatDate, formatTimeDiff, logger} from "../utils";

export class SessionStatisticsCounter {
  public successRequests: number;
  public failedRequests: number;
  public failedRequestsInRow: number;
  public successRequestsInRow: number;
  public finishedRequestsStatuses: {unknown: number} & {[key: string]: number} = { unknown: 0 };
  public startedTime: Date;
  public target: string;

  constructor() {
    this.reset();
  }

  setTarget(target: string) {
    this.target = target;
  }

  reset() {
    this.successRequests = 0;
    this.successRequestsInRow = 0;
    this.failedRequests = 0;
    this.failedRequestsInRow = 0;
    this.startedTime = new Date();
  }

  recordStatus(status?: number) {
    if ( status ) {
      const statusKey = `${status}`;
      if ( !this.finishedRequestsStatuses[statusKey] ) {
        this.finishedRequestsStatuses[statusKey] = 1;
      } else {
        this.finishedRequestsStatuses[statusKey]++;
      }
    } else {
      this.finishedRequestsStatuses.unknown++;
    }
  }

  success(status?: number) {
    this.recordStatus(status);
    this.successRequests++;
    this.successRequestsInRow++;
    this.failedRequestsInRow = 0;
  }

  fail(status?: number) {
    this.recordStatus(status);
    this.failedRequests++;
    this.failedRequestsInRow++;
    this.successRequestsInRow = 0;

    return this.failedRequestsInRow >= config.load.expectedFailsAmount;
  }

  log() {
    logger
        .clean('stats')
        .mark('stats')
        .line()
        .important('Session stats for', this.target)
        .info('\tStarted at: ', formatDate(this.startedTime))
        .info('\tWorking for:', formatTimeDiff(+new Date() - +this.startedTime),
            `in ${config.load.concurrentRequests} threads`)
        .info('\tStatuses:\t', this.finishedRequestsStatuses)
        .success('\tSuccess responses (200s):')
        .info('\t\ttotal:\t', this.successRequests)
        .info('\t\tin row:\t', this.successRequestsInRow)
        .warning('\tFail responses:')
        .info('\t\ttotal:\t', this.failedRequests)
        .info('\t\tin row:\t', this.failedRequestsInRow)
  }
}

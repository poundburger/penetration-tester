import {config, httpReqWithProxy, IProxy, ProxyManager} from "../proxy";
import {
  axiosRespSuccess,
  formatDate,
  formatTimeDiff,
  logger,
  promiseAnyWithValue,
  summaryLogger,
  timeout
} from "../utils";
import {loadTesterGreenThread} from "./load-tester-green-thread";
import {ProcessManager} from "./process-manager";

export class Tester {
  private processManager = new ProcessManager({
    timeoutMs: config.load.tickLengthMs
  });

  constructor(private proxyManager: ProxyManager, private targets: string[]) {
  }

  async init() {
    return this;
  }

  async run() {
    await this.testTick();
    await timeout(config.load.ticksPauseMs);
  }

  async testTick() {
    logger
        .line()
        .info(`Starting ${formatTimeDiff(config.load.tickLengthMs)} test session`);

    const proxyList = await this.proxyManager.getWorkingProxyList();
    if (!proxyList.length) {
      logger.error(`No working proxy found. Retry in ${formatTimeDiff(config.load.ticksPauseMs)}...`);
      return;
    }
    logger
        .success(`Total of ${proxyList.length} working proxy.`)
        .line();

    const targetsUp: string[] = [];
    const targetsDown: string[] = [];
    const targetSet = await promiseAnyWithValue<[string, IProxy]>(this.targets.map(async t => {
      const p = await this.getWorkingProxyPerTarget(t, proxyList);
      if (p) {
        targetsUp.push(t);
        return [t, p];
      }
      targetsDown.push(t);
      return;
    }));


    if (targetSet) {
      const [target, proxy] = targetSet;
      logger.info(`TEST`, target, 'with', proxy);
      await this.testLoad(target, proxy);
    }

    summaryLogger
        .clean()
        .mark()
        .success('DOWN', targetsDown)
        .warning('UP', targetsUp)
        .success(`Test session finished at ${formatDate(new Date())}`);
  }

  async getWorkingProxyPerTarget(target: string, proxyList: IProxy[]): Promise<IProxy | false> {
    return await promiseAnyWithValue(proxyList.map(async proxy =>
        await this._checkTargetUpPerProxy(target, proxy) ? proxy : false));
  }

  async _checkTargetUpPerProxy(target: string, proxy: IProxy): Promise<boolean> {
    return !!axiosRespSuccess(await httpReqWithProxy(target, proxy));
  }

  async testLoad(target: string, proxy: IProxy): Promise<void> {
    this.processManager.setTarget(target).start();

    await Promise.all(new Array(config.load.concurrentRequests)
        .fill(null).map(async () => await loadTesterGreenThread(
            target,
            proxy,
            this.processManager
        )));
    this.processManager.stop().finalizeLog();
  }
}

import cluster from "cluster";
import {config} from "./config";
import {logger} from "./utils";
import {worker} from "./worker";
import fs from "fs";
import path from "path";

async function main() {
  if (cluster.isPrimary || cluster.isMaster) {
    process.stdout.write(`Primary process ${process.pid} is running...\n`);
    process.stdout.write(`To read logs run "npm run readLogs"\n`);
    fs.writeFileSync(path.resolve(process.cwd(), 'pid.log'), `${process.pid}`, {encoding: 'utf8'});
    (cluster.setupPrimary ?? cluster.setupMaster)({
      execArgv: [`--max-old-space-size=${config.misc.processMemoryLimitMb}`]
    });
    cluster.on('exit', (worker) => {
      // logger.important(`Worker ${worker.process.pid} has died. Restarting...`);
      cluster.fork();
    });
    cluster.fork();
  } else if (cluster.isWorker) {
    logger
        .clean('worker')
        .mark('worker');
    await worker();
  }
}

main();

import {getOpenVPNConfigs, IOpenVPNConfigMapping} from "./get-openvpn-configs";
import {getProxyList} from "./get-proxy-list";
import {IProxy, IProxyEnriched, IResolveTest, IProxyMapping} from "./types";
import {config, locationTestUrl} from "./config";
import {validateProxy} from "./validate-proxy";
import {httpReqAxios, logger} from "../utils";

export class ProxyManager {
  public realAddress: IResolveTest;
  public proxyMapping: IProxyMapping;
  public OpenVPNConfigMapping: IOpenVPNConfigMapping;

  async init() {
    this.realAddress = await this.getAddressNoProxy(); // every proxy is to be checked against this data to verify if it works
    logger.info(`Real address information:`, this.realAddress);

    await this.refreshProxies();
    // await this.refreshVPNHosts(); // WIP
    return this;
  }

  async refreshProxies() {
    this.proxyMapping = await getProxyList();
  }

  async refreshVPNHosts() {
    this.OpenVPNConfigMapping = await getOpenVPNConfigs();
  }

  async validateProxy(proxy: IProxy, region: string): Promise<IResolveTest | false> {
    return await validateProxy(proxy, region, this.realAddress);
  }

  async getAddressNoProxy(): Promise<IResolveTest> {
    const resp = await httpReqAxios(locationTestUrl);
    if (!resp || !resp.data) {
      const errMessage = `Fatal: could not retrieve address from ${locationTestUrl}`;
      logger.error(errMessage);
      throw new Error(errMessage);
    }
    return resp.data;
  }

  async getWorkingProxyList(): Promise<IProxyEnriched[]> {
    return (await Promise.all(config.proxy.regions
        .map(async region => this.getWorkingProxyListPerRegion(region))))
        .reduce((acc, list) => acc.concat(list), []);
  }

  async getWorkingProxyListPerRegion(region: string): Promise<IProxyEnriched[]> {
    const proxyList = this.proxyMapping[region];
    return (await Promise.all(proxyList.map(async proxy => {
      const resolveTest = await this.validateProxy(proxy, region);

      if (resolveTest) {
        return {...proxy, resolveTest};
      }
    }))).filter(v => v);
  }
}

export interface IResolveTest {
  ip: string;
  country: string;
  cc: string;
}

export interface IProxy {
  ip: string,
  port: number,
  types: {
    HTTP?: boolean,
    HTTPS?: boolean,
    SOCKS4?: boolean,
    SOCKS5?: boolean,
  },
}
export type IProxyEnriched = IProxy & { resolveTest: IResolveTest };

export interface IProxyMapping {
  [ countryCode: string ]: IProxy[];
}
